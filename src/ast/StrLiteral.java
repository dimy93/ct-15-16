package ast;

public class StrLiteral extends Expr{
    public final String value;

    public StrLiteral( String value ) {
		this.value = value;
	}

	@Override
	public <T> T accept(ASTVisitor<T> v) {
    	return (T)v.visitStrLiteral(this);
	}
}