package ast;

import java.io.PrintWriter;
import java.util.List;

public class ASTPrinter implements ASTVisitor<Void> {

    private PrintWriter writer;

    public ASTPrinter(PrintWriter writer) {
            this.writer = writer;
    }
    private void printList( List<?> list ){
        if ( list.isEmpty() ) return;
        for ( Object elem : list.subList(0,list.size()-1) ) {
            Tree ast = (Tree) elem;
            ast.accept(this);
            writer.print(",");
        }
        Tree ast = ( Tree ) list.get( list.size() - 1 );
        ast.accept(this);
    }
    public Void visitBlock(Block b) {
        writer.print("Block(");
        printList( b.vds );
        if ( !b.smts.isEmpty() && !b.vds.isEmpty() )
        {
           writer.print(",");
        }
        printList( b.smts );
        writer.print(")");
        return null;
    }

    public Void visitProcedure(Procedure p) {
        writer.print("Procedure(");
        writer.print(p.type);
        writer.print(","+p.name+",");
        for (VarDecl vd : p.params) {            
            vd.accept(this);
            writer.print(",");
        }
        p.block.accept(this);
        writer.print(")");
        return null;
    }

    public Void visitProgram(Program p) {
        writer.print("Program(");
        for (VarDecl vd : p.varDecls) {
            vd.accept(this);
            writer.print(",");
        }
        for (Procedure proc : p.procs) {
            proc.accept(this);
            writer.print(",");
        }
        p.main.accept(this);
        writer.print(")");
        writer.flush();
        return null;
    }

    public Void visitVarDecl(VarDecl vd){
        writer.print("VarDecl(");
        writer.print(vd.type+",");
        vd.var.accept(this);
        writer.print(")");
        return null;
    }

	public Void visitVar(Var v) {
		writer.print("Var(");
		writer.print(v.name);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitType(Type t) {
		writer.print(t.name());
		return null;
	}
 
	@Override
	public Void visitIntLiteral(IntLiteral il) {
		writer.print("IntLiteral("+il.value+")");
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral sl) {
		writer.print("StrLiteral("+sl.value+")");
		return null;
	}


	@Override
	public Void visitFunCallExpr(FunCallExpr fce) {
		writer.print("FunCallExpr(");
		writer.print(fce.name);
		if ( !fce.exprs.isEmpty() )
			writer.print(",");
		printList( fce.exprs );
		writer.print(")");
		return null;
	}

	@Override
	public Void visitBinOp(BinOp v) {
		writer.print("BinOp(");
		v.lhs.accept(this);
		writer.print(","+v.op.name()+",");
		v.rhs.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitFunCallStmt(FunCallStmt fcs) {
		writer.print("FunCallStmt(");
		writer.print(fcs.name);
		if ( !fcs.exprs.isEmpty() )
			writer.print(",");
		printList( fcs.exprs );
		writer.print(")");
		return null;
	}

	@Override
	public Void visitWhile(While wh) {
		writer.print("While(");
		wh.e.accept(this);
		writer.print(",");
		wh.stmt.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitIf(If i) {
		writer.print("If(");
		i.cond.accept(this);
		writer.print(",");
		i.if_.accept(this);
		if ( i.else_ != null ) {
			writer.print(",");
			i.else_.accept(this); 
		}
		writer.print(")");
		return null;
	}

	@Override
	public Void visitAssign(Assign as) {
		writer.print("Assign(");
		as.v.accept(this);
		writer.print(",");
		as.e.accept(this);
		writer.print(")");
		return null;
	}

	@Override
	public Void visitReturn(Return re) {
		writer.print("Return(");
		if ( re.e != null ) {
			re.e.accept(this); 
		}
		writer.print(")");
		return null;
	}

	@Override
	public Void visitCharLiteral(ChrLiteral cl) {
		writer.print("ChrLiteral("+cl.value+")");
		return null;
	}
    
}
