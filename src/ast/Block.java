package ast;

import java.util.List;

public class Block extends Stmt {
	final public List<VarDecl> vds;
	final public List<Stmt> smts;
	
	public Block(List<VarDecl> vds,List<Stmt> smts){
		this.vds = vds;
		this.smts = smts;
	}
	
    public <T> T accept(ASTVisitor<T> v) {
	    return (T)v.visitBlock(this);
    }
}
