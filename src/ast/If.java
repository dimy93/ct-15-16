package ast;

public class If extends Stmt {
	public final Expr cond;
	public final Stmt if_;
	public final Stmt else_;
	public If(Expr cond, Stmt if_, Stmt else_) {
		this.if_ = if_;
		this.else_ = else_;
		this.cond = cond;
	}

	@Override
	public <T> T accept(ASTVisitor<T> v) {
		return (T)v.visitIf(this);
	}

}
