package ast;

public class Assign extends Stmt {
    public final Var v;
    public final Expr e;
    public Assign(Var v, Expr e){
    	this.v = v;
    	this.e = e;
    }
	@Override
	public <T> T accept(ASTVisitor<T> v) {
		return (T)v.visitAssign(this);
	}
}
