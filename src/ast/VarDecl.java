package ast;

public class VarDecl implements Tree {
    public final Type type;
    public final Var var;
    public int label = -1;

    public VarDecl(Type type, Var var) {
    	this.type = type;
    	this.var = var;
    }

    public <T> T accept(ASTVisitor<T> v) {
    	return (T)v.visitVarDecl(this);
    }
}
