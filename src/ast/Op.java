package ast;

public enum Op {
	ADD("+"),
	SUB("-"),
	MUL("*"),
	DIV("/"),
	MOD("%"),
	GT(">"),
	LT("<"),
	GE(">="),
	LE("<="),
	NE("!="),
	EQ ("==");
	
	public final String name;
	private Op(String name) {
        this.name = name;
    }
}
