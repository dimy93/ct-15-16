package ast;

public class Return extends Stmt {
    public final Expr e;
    
    public Return(Expr e){
    	this.e = e;
    }
    
    public Return(){
    	this(null);
    }
    
	@Override
	public <T> T accept(ASTVisitor<T> v) {
		return (T)v.visitReturn(this);
	}
}