package ast;

public interface ASTVisitor<T> {
    public T visitBlock(Block b);
    public T visitProcedure(Procedure p);
    public T visitProgram(Program p);
    public T visitVarDecl(VarDecl vd);
    public T visitVar(Var v);
    public T visitType(Type t);
    public T visitIntLiteral(IntLiteral il);
    public T visitStrLiteral(StrLiteral sl);
    public T visitCharLiteral(ChrLiteral cl);
    public T visitFunCallExpr(FunCallExpr fce);
    public T visitBinOp(BinOp v);
    public T visitFunCallStmt(FunCallStmt fcs);
    public T visitWhile(While wh);
    public T visitIf(If i);
    public T visitAssign(Assign as);
    public T visitReturn(Return re);
}
