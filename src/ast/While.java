package ast;

public class While extends Stmt {
    public final Expr e;
    public final Stmt stmt;
    
	public While(Expr e, Stmt stmt) {
		this.e = e;
		this.stmt = stmt;
	}

	@Override
	public <T> T accept(ASTVisitor<T> v) {
	    return (T)v.visitWhile(this);
	}

}
