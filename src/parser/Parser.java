package parser;

import ast.*;
import lexer.Token;
import lexer.Tokeniser;
import lexer.Token.TokenClass;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;


/**
 * @author cdubach
 */
public class Parser {

    private Token token;

    // use for backtracking (useful for distinguishing decls from procs when parsing a program for instance)
    private Queue<Token> buffer = new LinkedList<>();

    private final Tokeniser tokeniser;



    public Parser(Tokeniser tokeniser) {
        this.tokeniser = tokeniser;
    }

    public Program parse() {
        // get the first token
        nextToken();

        return parseProgram();
    }

    public int getErrorCount() {
        return error;
    }

    private int error = 0;
    private Token lastErrorToken;

    private void error(TokenClass... expected) {
        if (lastErrorToken == token) {
            // skip this error, same token causing trouble
            return;
        }

        StringBuilder sb = new StringBuilder();
        String sep = "";
        for (TokenClass e : expected) {
            sb.append(sep);
            sb.append(e);
            sep = "|";
        }

        error++;
        lastErrorToken = token;
    }

    /*
     * Look ahead the i^th element from the stream of token.
     * i should be >= 1
     */
    private Token lookAhead(int i) {
        // ensures the buffer has the element we want to look ahead
        while (buffer.size() < i)
            buffer.add(tokeniser.nextToken());
        assert buffer.size() >= i;

        int cnt=1;
        for (Token t : buffer) {
            if (cnt == i)
                return t;
            cnt++;
        }

        assert false; // should never reach this
        return null;
    }


    /*
     * Consumes the next token from the tokeniser or the buffer if not empty.
     */
    private void nextToken() {
        if (!buffer.isEmpty())
            token = buffer.remove();
        else
            token = tokeniser.nextToken();
    }

    /*
     * If the current token is equals to the expected one, then skip it, otherwise report an error.
     * Returns the expected token or null if an error occurred.
     */
    private Token expect(TokenClass... expected) {
        for (TokenClass e : expected) {
            if (e.equals(token.tokenClass)) {
                Token cur = token;
                nextToken();
                return cur;
            }
        }
        error(expected);
        nextToken();
        return null;
    }

    /*
    * Returns true if the current token is equals to any of the expected ones.
    */

    private boolean accept(TokenClass... expected) {
        boolean result = false;
        for (TokenClass e : expected)
            result |= (e.equals(token.tokenClass));
        return result;
    }
    
    /*
    * Returns true if the future token is equals to any of the expected ones.
    */

    private boolean accept( int i, TokenClass... expected) {
    	Token token = lookAhead(i);
        boolean result = false;
        for (TokenClass e : expected)
            result |= (e == token.tokenClass);
        return result;
    }


    private Program parseProgram() {
        parseIncludes();
        List<VarDecl> varDecls = parseDecls();
        List<Procedure> procs = parseProcs();
        Procedure main = parseMain();
        expect(TokenClass.EOF);
        return new Program(varDecls, procs, main);
    }

    // includes are ignored, so does not need to return an AST node
    private void parseIncludes() {
	    if (accept(TokenClass.INCLUDE)) {
            nextToken();
            expect(TokenClass.STRING_LITERAL);
            parseIncludes();
        }
    }

    private List<VarDecl> parseDecls()  {
    	TokenClass[] first = { TokenClass.VOID, TokenClass.INT, TokenClass.CHAR };
    	TokenClass[] second = { TokenClass.IDENTIFIER };
    	TokenClass[] third = { TokenClass.SEMICOLON };
    	if ( accept( first ) && accept( 1, second ) &&  accept( 2,third ) ){ //LL3 lookup to differentiate Procs from Declr
    		VarDecl vd = parseTypeIdent();
    		expect(TokenClass.SEMICOLON);
    		List<VarDecl>vds = parseDecls();
    		vds.add(0,vd);
    		return vds;
    	}
    	return new ArrayList<VarDecl>();
    }
    private List<Procedure> parseProcs() {
    	TokenClass[] first = { TokenClass.VOID, TokenClass.INT, TokenClass.CHAR };
    	TokenClass[] second = { TokenClass.IDENTIFIER };
    	TokenClass[] third = { TokenClass.LPAR };
    	if ( accept( first ) && accept( 1, second ) &&  accept( 2, third ) ){ //LL3 lookup to differentiate Procs from Declr
    		Procedure proc = parseProcedure();
    		List<Procedure> procs = parseProcs();
    		procs.add(0,proc);
    		return procs;
    	}
    	return new ArrayList<Procedure>();
    }

    private Procedure parseMain() {
    	expect(TokenClass.VOID);
    	expect(TokenClass.MAIN);
    	expect(TokenClass.LPAR);
    	expect(TokenClass.RPAR);
    	Block bl = parseBody();
    	return new Procedure(Type.VOID, "main", new ArrayList<VarDecl>(), bl);
    }
    
    private VarDecl parseTypeIdent() {
    	Type t = parseType();
    	Var v = new Var(token.data);
    	expect(TokenClass.IDENTIFIER);
    	return new VarDecl(t,v);
    }
    
    private Procedure parseProcedure() {
    	Type t = parseType();
    	String name = token.data;
    	expect(TokenClass.IDENTIFIER);
    	expect(TokenClass.LPAR);
    	List<VarDecl> vds = parseParams();
    	expect(TokenClass.RPAR);
    	Block bl = parseBody();
    	return new Procedure( t, name, vds, bl );
    }
    
    private Block parseBody() {
    	expect(TokenClass.LBRA);
    	List<VarDecl> vds = parseDecls();
    	List<Stmt> stmts = parseStmtList();
    	expect(TokenClass.RBRA);
		return new Block(vds,stmts);
    }
    
    private Type parseType() {
    	Type t;
    	switch(token.tokenClass){
    		case CHAR: t = Type.CHAR; break;
    		case INT:  t = Type.INT;  break;
    		case VOID: t = Type.VOID; break;
		    default:   t = null; break; 
    	}
    	expect( TokenClass.CHAR, TokenClass.INT, TokenClass.VOID );
    	return t;
    }
    
    private List<VarDecl> parseParams(){
    	if ( accept( TokenClass.RPAR ) ){ // Folows(params)={"}"}
    		return new ArrayList<VarDecl>();
    	}
    	VarDecl vd = parseTypeIdent();
    	List<VarDecl> vds = parseParamsRep();
    	vds.add(0,vd);
    	return vds;
    }
    
    private Stmt parseStmt(){
      // Do not consume ID token for funcalls
      if ( accept( TokenClass.IDENTIFIER ) && !accept( 1, TokenClass.ASSIGN ) ){
         FunCallStmt fcs = parseFunCallStmt();
         expect(TokenClass.SEMICOLON);
         return fcs;
      }
      if ( accept( TokenClass.IDENTIFIER ) && accept( 1, TokenClass.ASSIGN ) ){
    	    Var v = new Var(token.data);
    	    expect(TokenClass.IDENTIFIER);
            expect(TokenClass.ASSIGN);
  		    Expr e = parseLExpr();
  			expect(TokenClass.SEMICOLON);
  			return new Assign(v,e);
      }


      TokenClass[] accepted = { TokenClass.LBRA,TokenClass.WHILE,TokenClass.IF,TokenClass.IDENTIFIER,TokenClass.RETURN,TokenClass.PRINT,TokenClass.READ };
     	Token to = token;
      expect(accepted);
    	switch ( to.tokenClass ){
	    	case LBRA:       List<VarDecl> vds = parseDecls();
	    					 List<Stmt> stmts = parseStmtList();
	    	                 expect(TokenClass.RBRA);
	    	                 return new Block(vds,stmts);
	    	case WHILE:      expect(TokenClass.LPAR);
	    	                 Expr e = parseExpr();
	    	                 expect(TokenClass.RPAR);
	    	                 Stmt stmt = parseStmt();
	    	                 return new While(e,stmt);
	    	case IF:         expect(TokenClass.LPAR);
	    	                 Expr cond = parseExpr();
	    	                 expect(TokenClass.RPAR);
	    	                 Stmt s1 = parseStmt();
	    	                 Stmt s2 = null;
	    	                 if(accept(TokenClass.ELSE)){
	    	                	 nextToken();
	    	                	 s2 = parseStmt();
	    	                 };
	    	                 return new If(cond,s1,s2);

	    	case RETURN:     if(accept(TokenClass.SEMICOLON)){
	    					 	nextToken();
	    					 	return new Return();
	    					 }
	    					 Expr ret = parseLExpr();
	    					 expect(TokenClass.SEMICOLON);
	    					 return new Return(ret);
	    	case PRINT:      if ( to.data.equals("print_s") ){
	    						expect(TokenClass.LPAR);
	    						List<Expr> prt_arg = new ArrayList<Expr>();
	    						prt_arg.add(new StrLiteral(token.data));
	    						expect(TokenClass.STRING_LITERAL);
	    						expect(TokenClass.RPAR);
	    						expect(TokenClass.SEMICOLON);
	    						return new FunCallStmt( to.data, prt_arg );
	    					 }
	    					 expect(TokenClass.LPAR);
	    				     List<Expr> prt_arg = new ArrayList<Expr>();
	    				     prt_arg.add(parseLExpr());
	    					 expect(TokenClass.RPAR);
	    					 expect(TokenClass.SEMICOLON);
	    					 return new FunCallStmt( to.data, prt_arg );
	    	case READ:       expect(TokenClass.LPAR);
	    	                 expect(TokenClass.RPAR);
	    	                 expect(TokenClass.SEMICOLON);
	    	                 return new FunCallStmt( to.data, new ArrayList<Expr>() ) ;
    	}
		return null;
    }
    
	private List<VarDecl> parseParamsRep(){
    // No forward lookup required since Follows(paramsRep) = {")"} which doesn't contains ","
    	if ( accept(TokenClass.COMMA) ){
    		nextToken();
    		VarDecl vd = parseTypeIdent();
    		List<VarDecl> vds = parseParamsRep();
    		vds.add(0,vd);
    		return vds;
    	}
    	return new ArrayList<VarDecl>();
    }
    
    private Expr parseExpr(){
    	Expr lhs = parseLExpr();
    	TokenClass[] boolExpr = { TokenClass.LT, TokenClass.GT, TokenClass.LE, TokenClass.GE, TokenClass.EQ, TokenClass.NE };
    	if ( !accept(boolExpr) ) // No forward lookup because booleanOp are only presented in expr and therefore not possible to be in Follows(expr)
    		return lhs;
    	Op operation = null;
    	switch(token.tokenClass){
    		case LT: operation = Op.LT; break;
    		case GT: operation = Op.GT; break;
    		case LE: operation = Op.LE; break;
    		case GE: operation = Op.GE; break;
    		case EQ: operation = Op.EQ; break;
    		case NE: operation = Op.NE; break;
    	}
    	nextToken();
    	Expr rhs = parseLExpr();
    	return new BinOp(lhs, operation, rhs);
    	
    }
    
    private List<Stmt> parseStmtList(){
      // Follows(stmtList) = {"}"}
      TokenClass[] stmtFirst = { TokenClass.LBRA, TokenClass.WHILE, TokenClass.IF, TokenClass.IDENTIFIER, TokenClass.RETURN, TokenClass.PRINT, TokenClass.READ };
      if (accept(stmtFirst)){
    	   Stmt stmt = parseStmt();
    	   List<Stmt> stmts = parseStmtList();
    	   stmts.add(0,stmt);
    	   return stmts;
      }
      return new ArrayList<Stmt>();
    }
    private Expr parseLExpr(){
    	Expr lhs = parseTerm();
    	Op op = null;
    	switch( token.tokenClass ){
    		case PLUS: op = Op.ADD; break;
    		case MINUS: op = Op.SUB; break;
    	}
    	Expr rhs = parseLExpRep();
    	if ( op != null )
    		return new BinOp(lhs,op,rhs);
    	return lhs;
    }
    
    @SuppressWarnings("unchecked")
	private FunCallStmt parseFunCallStmt(){
    	String name = token.data;
      expect(TokenClass.IDENTIFIER);
    	expect(TokenClass.LPAR);
    	List<Expr> exprs = (List<Expr>)(List<?>)parseArgList();
    	expect(TokenClass.RPAR);
    	return new FunCallStmt(name,exprs);
    }
    
    @SuppressWarnings("unchecked")
	private FunCallExpr parseFunCallExpr(){
    	String name = token.data;
    	expect(TokenClass.IDENTIFIER);
    	expect(TokenClass.LPAR);
    	List<Expr> exprs= (List<Expr>)(List<?>)parseArgList();
    	expect(TokenClass.RPAR);
    	return new FunCallExpr(name,exprs);
    }
    
    private Expr parseTerm(){
    	Expr lhs = parseFactor();
    	Op op = null;
    	switch( token.tokenClass ){
    		case TIMES: op = Op.MUL; break;
    		case DIV:   op = Op.DIV; break;
    		case MOD:   op = Op.MOD; break;
    	}
    	Expr rhs = parseTermRep();
    	if ( op != null )
    		return new BinOp(lhs,op,rhs);
    	return lhs;
    }
    
    private Expr parseLExpRep(){
    	// Follows(lexprep) = {")",booleanOp,";"} which doesn't contain + or - 
    	TokenClass[] sign = { TokenClass.PLUS, TokenClass.MINUS };
    	if ( !accept(sign) )
    		return null;
        nextToken();
    	Expr lhs = parseTerm();
    	Op op = null;
    	switch( token.tokenClass ){
    		case PLUS:  op = Op.ADD; break;
    		case MINUS: op = Op.SUB; break;
    	}
    	Expr rhs = parseLExpRep();
    	if ( op != null )
    		return new BinOp(lhs,op,rhs);
    	return lhs;
    } 
    
    private List<Var> parseArgList(){
    	// Follows(arglist) = {")"} => doesn't contain Identifiers
    	if ( accept(TokenClass.IDENTIFIER) ){
    		Var v = new Var(token.data);
    		nextToken();
    		List<Var> args = parseArgRep();
    		args.add(0,v);
			return args;
    	}
    	return new ArrayList<Var>();
    }
    
    private Expr parseFactor(){
		TokenClass[] accepted = { TokenClass.LPAR, TokenClass.MINUS, TokenClass.IDENTIFIER, TokenClass.NUMBER, TokenClass.CHARACTER, TokenClass.READ };
		TokenClass cl = token.tokenClass;
		String data = token.data;
		// Follows(factor,ID) = {mathOp,boolOp,")"} which doesn't contain "(" so we can use LL2 to differentiate them
		if ( accept( TokenClass.IDENTIFIER ) && accept( 1, TokenClass.LPAR ) ){
			return parseFunCallExpr();
		}
		expect( accepted );
		switch ( cl ){
	    	case LPAR:       Expr e = parseLExpr();
	    	                 expect(TokenClass.RPAR);
	    	                 return e;
	    	case MINUS:      Expr minus_arg;
	    	                 if ( token.tokenClass == TokenClass.IDENTIFIER ){
	    	                	 minus_arg = new Var(token.data);
	    	                 }
	    	                 else {
	    	                	 minus_arg = new IntLiteral(Integer.parseInt(token.data));
	    	                 }
	    		             expect(TokenClass.IDENTIFIER,TokenClass.NUMBER); 
                             return new BinOp(new IntLiteral(0), Op.SUB, minus_arg);
			case IDENTIFIER: return new Var(data);
			case CHARACTER:  return new ChrLiteral(data.charAt(0));
			case NUMBER:     return new IntLiteral( Integer.parseInt(data) );
			case READ:       expect(TokenClass.LPAR);
			                 expect(TokenClass.RPAR);
			                 return new FunCallExpr(data,new ArrayList<Expr>());	 
		}
		return null;
    }
    
    private List<Var> parseArgRep(){
    	// Follows(argRep) = {")"} => doesn't contain comma  
    	if ( accept(TokenClass.COMMA) ){
    		nextToken();
    		Var v = new Var(token.data);
    		expect(TokenClass.IDENTIFIER);
    		List<Var> vs = parseArgRep();
    		vs.add(0,v);
    		return vs;
    	}
    	return new ArrayList<Var>();
    }
    
    private Expr parseTermRep(){
    	// Follows(termRep)={+,-,")",boolOp,;} which doesn't contain divOps
    	TokenClass[] divOp = { TokenClass.DIV, TokenClass.MOD, TokenClass.TIMES };
    	if ( accept(divOp) ){
    		nextToken();
    		Expr lhs = parseFactor();
        	Op op = null;
        	switch( token.tokenClass ){
        		case TIMES: op = Op.MUL; break;
        		case DIV:   op = Op.DIV; break;
        		case MOD:   op = Op.MOD; break;
        	}
        	Expr rhs = parseTermRep();
        	if ( op != null )
        		return new BinOp(lhs,op,rhs);
        	return lhs;
    	}
    	return null;
    }    
}
