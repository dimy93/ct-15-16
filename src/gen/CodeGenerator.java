package gen;

import java.io.FileOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.objectweb.asm.*;

import ast.ASTVisitor;
import ast.Assign;
import ast.BinOp;
import ast.Block;
import ast.ChrLiteral;
import ast.Expr;
import ast.FunCallExpr;
import ast.FunCallStmt;
import ast.If;
import ast.IntLiteral;
import ast.Procedure;
import ast.Program;
import ast.Return;
import ast.Stmt;
import ast.StrLiteral;
import ast.Type;
import ast.Var;
import ast.VarDecl;
import ast.While;

public class CodeGenerator implements ASTVisitor<Void>, Opcodes{

    protected FileOutputStream writer;
    protected ClassWriter code_emitter;
	protected MethodVisitor mv; // Method context
	protected int varsOnStack;  

    public CodeGenerator() {
      try{
        writer = new FileOutputStream("out/Main.class");
      } catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
    }

	public void emitProgram(Program program) {
        // TODO: emit a java class named Main that contains your program and write it to the file out/Main.class
        // to do this you will need to write a visitor which traverses the AST and emit the different global variables as static fields and the different procedures as static methods.
		program.accept(this);
		try {
			writer.write(code_emitter.toByteArray());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	@Override
	public Void visitProgram(Program p) {
		code_emitter =  new ClassWriter(ClassWriter.COMPUTE_FRAMES);
        // Create class
		code_emitter.visit(V1_7, ACC_PUBLIC + ACC_SUPER, "Main", null, 
				                               "java/lang/Object", null);
		// Constructor of the class - does nothing
		{
			mv = code_emitter.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
			mv.visitCode();
			mv.visitVarInsn(ALOAD, 0);
			mv.visitMethodInsn(INVOKESPECIAL, "java/lang/Object", "<init>", "()V");
			mv.visitInsn(RETURN);
			mv.visitMaxs(1, 1);
			mv.visitEnd();
		}
		for( VarDecl vd : p.varDecls ){
			code_emitter.visitField(ACC_PUBLIC+ACC_STATIC, vd.var.name, convertType(vd.type), null, null).visitEnd();
		}
		for( Procedure pr : p.procs ){
			pr.accept(this);
		}
		p.main.accept(this);
		code_emitter.visitEnd();
		return null;
	}

	@Override
	public Void visitBlock(Block b) {
		for( VarDecl vd : b.vds ){
			vd.accept(this);
		}
		for( Stmt stmt : b.smts ){
			stmt.accept(this);
		}
		return null;
	}

	@Override
	public Void visitProcedure(Procedure p) {
		String funcType = "(";
		for ( VarDecl param : p.params ){
			funcType = funcType + convertType(param.type);
			param.label = varsOnStack;
			varsOnStack += 2;
			if ( param.type == Type.STRING )
				varsOnStack--;
		}
		funcType = funcType + ")" + convertType(p.type); 
		if (p.name.equals("main")){
			funcType = "([Ljava/lang/String;)V";
			varsOnStack = 1;
		}
		mv = code_emitter.visitMethod(ACC_PUBLIC+ACC_STATIC, p.name, funcType, null, null);
		mv.visitCode();
		p.block.accept(this);
		if(p.type == Type.VOID){
			mv.visitInsn(RETURN);
		}else{
			mv.visitLdcInsn(new Long(0));
			mv.visitInsn(LRETURN);
		}
		mv.visitMaxs(1, 1);
		mv.visitEnd();
		varsOnStack = 0;
		return null;
	}

	@Override
	public Void visitVarDecl(VarDecl vd) {
		vd.label = varsOnStack;
		mv.visitLdcInsn(new Long(0));
		mv.visitVarInsn(LSTORE, varsOnStack);
		varsOnStack+=2;
		return null;
	}

	@Override
	public Void visitVar(Var v) {
		if ( v.vd.label != -1 ){ 
			mv.visitVarInsn(LLOAD, v.vd.label);
		}else{
			mv.visitFieldInsn(GETSTATIC, "Main", v.name, convertType(v.type));
		}
		return null;
	}

	@Override
	public Void visitType(Type t) {
		return null;
	}

	@Override
	public Void visitIntLiteral(IntLiteral il) {
		mv.visitLdcInsn(new Long(il.value));
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral sl) {
		mv.visitLdcInsn(sl.value);
		return null;
	}

	@Override
	public Void visitCharLiteral(ChrLiteral cl) {
		mv.visitLdcInsn(new Long(cl.value));
		return null;
	}

	@Override
	public Void visitFunCallExpr(FunCallExpr fce) {
		// Needs read
		String funcType = "(";
		for(Expr arg :fce.exprs){
			funcType = funcType + convertType(arg.type);
			arg.accept(this);
		}
		funcType = funcType + ")" + convertType(fce.p.type); 
		switch (fce.p.name) {
			case "read_c" : { 
				mv.visitMethodInsn(INVOKESTATIC, "IO", "read_c", "()C");
				mv.visitInsn(I2L);
				break;
			}
			case "read_i" : { 
				mv.visitMethodInsn(INVOKESTATIC, "IO", "read_i", "()I"); 
				mv.visitInsn(I2L);
				break;
			}
			default : {
				mv.visitMethodInsn(INVOKESTATIC, "Main", fce.name, funcType); 
				break;
			}
		}
		return null;
	}

	@Override
	public Void visitBinOp(BinOp binop) {
		binop.lhs.accept(this);
		binop.rhs.accept(this);
		switch (binop.op){
			case ADD: mv.visitInsn(LADD); break;
			case DIV: mv.visitInsn(LDIV); break;
			case MOD: mv.visitInsn(LREM); break;
			case MUL: mv.visitInsn(LMUL); break;
			case SUB: mv.visitInsn(LSUB); break;
			case EQ:  { 
				mv.visitInsn(LCMP);
				mv.visitInsn(I2L);
				mv.visitInsn(DUP2); 
				
				// Check if -1
				mv.visitLdcInsn(new Long(-1)); 
				mv.visitInsn(LXOR); 
				
				// LSWAP
				mv.visitInsn(DUP2_X2); 
				mv.visitInsn(POP2);
				
				// Check if 1
				mv.visitLdcInsn(new Long(1)); 
				mv.visitInsn(LXOR); 
				
				// Check if 1 or -1
				mv.visitInsn(LAND);
				break;
			}
			case NE:  mv.visitInsn(LXOR); break;
			case GT:  { // make GT from LT by switching the aruments
				mv.visitInsn(DUP2_X2);
				mv.visitInsn(POP2);
				mv.visitInsn(LSUB);
				mv.visitLdcInsn(new Long(1<<31)); 
				mv.visitInsn(LAND);
				break;
			}
			case LT:{ // make LT by substracting and checking the sign
				mv.visitInsn(LSUB);
				mv.visitLdcInsn(new Long(1<<31)); 
				mv.visitInsn(LAND);
				break;
			}
			case LE: {
				mv.visitInsn(LCMP);
				mv.visitInsn(I2L); 
				
				// Check if not 1
				mv.visitLdcInsn(new Long(1)); 
				mv.visitInsn(LXOR); 
				break;
			}
			case GE: { 
				mv.visitInsn(LCMP);
				mv.visitInsn(I2L); 
			
				// Check if not -1
				mv.visitLdcInsn(new Long(-1)); 
				mv.visitInsn(LXOR); 
				break;
			}
			default:
				break;
		}
		return null;
	}

	@Override
	public Void visitFunCallStmt(FunCallStmt fcs) {
		// Needs a read
		String funcType = "(";
		for(Expr arg :fcs.exprs){
			funcType = funcType + convertType(arg.type);
			arg.accept(this);
		}
		funcType = funcType + ")" + convertType(fcs.p.type); 
		switch (fcs.p.name) {
			case "print_i": mv.visitInsn(L2I); mv.visitMethodInsn(INVOKESTATIC, "IO", "print_i", "(I)V"); break;
			case "print_c": mv.visitInsn(L2I); mv.visitInsn(I2C); mv.visitMethodInsn(INVOKESTATIC, "IO", "print_c", "(C)V"); break;
			case "print_s": mv.visitMethodInsn(INVOKESTATIC, "IO", "print_s", "(Ljava/lang/String;)V"); break;
			case "read_c" : { 
				mv.visitMethodInsn(INVOKESTATIC, "IO", "read_c", "()C"); 
				mv.visitInsn(POP);
				break;
			}
			case "read_i" : { 
				mv.visitMethodInsn(INVOKESTATIC, "IO", "read_i", "()I"); 
				mv.visitInsn(POP);
				break;
			}
			default : {
				mv.visitMethodInsn(INVOKESTATIC, "Main", fcs.name, funcType); 
				if ( fcs.p.type != Type.VOID ){
					mv.visitInsn(POP2);
				}
				break;
			}
		}
		return null;
	}

	@Override
	public Void visitWhile(While wh) {
		Label cond = new Label();
		Label exit = new Label();
		mv.visitLabel(cond);
		wh.e.accept(this);
		mv.visitLdcInsn(new Long(0));
		mv.visitInsn(LCMP);
		mv.visitJumpInsn(IFEQ, exit);
		wh.stmt.accept(this);
		mv.visitJumpInsn(GOTO, cond);
		mv.visitLabel(exit);
		return null;
	}

	@Override
	public Void visitIf(If i) {
		Label else_ = new Label();
		Label exit = new Label();
		i.cond.accept(this);
		mv.visitLdcInsn(new Long(0));
		mv.visitInsn(LCMP);
		mv.visitJumpInsn(IFEQ, else_);
		i.if_.accept(this);
		mv.visitJumpInsn(GOTO, exit);
		mv.visitLabel(else_);
		if ( i.else_ != null ){
			i.else_.accept(this);
		}
		mv.visitLabel(exit);
		return null;
	}

	@Override
	public Void visitAssign(Assign as) {
		as.e.accept(this);
		if (as.v.vd.label != -1) {
			mv.visitVarInsn(LSTORE, as.v.vd.label);
		} else {
			mv.visitFieldInsn(PUTSTATIC, "Main", as.v.name, convertType(as.v.type));
		}
		return null;
	}

	@Override
	public Void visitReturn(Return re) {
		if(re.e != null){
			re.e.accept(this);
			mv.visitInsn(LRETURN);
		}else{
			mv.visitInsn(RETURN);
		}
		return null;
	}
	private String convertType(Type t){
		switch (t){
			case INT: return "J"; 
			case CHAR: return "J";
			case STRING: return "Ljava/lang/String;";
			case VOID: return "V";
		}
		return null;
	}
}
