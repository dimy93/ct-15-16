package sem;

import ast.*;

public class TypeCheckVisitor extends BaseSemanticVisitor<Type> {
	private Procedure keepTrackOfProcForReturn;
	@Override
	public Type visitBlock(Block b) {
		for( VarDecl vd : b.vds ){
			vd.accept(this);
		}
		for( Stmt stmt : b.smts ){
			stmt.accept(this);
		}
		return null;
	}

	@Override
	public Type visitProcedure(Procedure p) {
		keepTrackOfProcForReturn = p;
		for( VarDecl vd : p.params ){
			vd.accept(this);
		}
		p.block.accept(this);
		keepTrackOfProcForReturn = null;
		return null;
	}

	@Override
	public Type visitProgram(Program p) {
		for( VarDecl vd : p.varDecls ){
			vd.accept(this);
		}
		for( Procedure pr : p.procs ){
			pr.accept(this);
		}
		p.main.accept(this);
		return null;
	}

	@Override
	public Type visitVarDecl( VarDecl vd ){
		if( vd.type == Type.VOID )
			error("Cannot declarate vars from type void");
		return null;
	}

	@Override
	public Type visitType(Type t) {
		return null;
	}

	@Override
	public Type visitIntLiteral(IntLiteral il) {
		il.type = Type.INT;
		return Type.INT;
	}

	@Override
	public Type visitStrLiteral(StrLiteral sl) {
		sl.type = Type.STRING;
		return Type.STRING;
	}

	@Override
	public Type visitCharLiteral(ChrLiteral cl) {
		cl.type = Type.CHAR;
		return Type.CHAR;
	}

	@Override
	public Type visitFunCallExpr(FunCallExpr fce) {
		if ( fce.p == null ) {
			return null;
		}
		if ( fce.p.type == Type.VOID ){
			error("Function " + fce.name + "returns void but we expect char or int" );
			fce.type = fce.p.type;
			return fce.p.type;
		}
		if ( fce.p.params.size() != fce.exprs.size() ) {
			error("Wrong number of args for function " + fce.name );
			fce.type = fce.p.type;
			return fce.p.type;
		}
		int i = 0;
		for ( Expr e : fce.exprs ){
			Type argTsup = e.accept(this);
			Type argTreq = fce.p.params.get(i).type;
			if ( argTsup != argTreq ){
				error("Arg" + i + " in function " + fce.name + ": Expected " + argTreq + " but found " + argTsup );
			}
			i++;
		}
		fce.type = fce.p.type;
		return fce.p.type;
	}

	@Override
	public Type visitBinOp( BinOp bo ){
		Type lhsT = bo.lhs.accept(this);
		Type rhsT = bo.rhs.accept(this);
		if( bo.op == Op.ADD || bo.op == Op.SUB || bo.op == Op.MUL || bo.op == Op.DIV ||  bo.op == Op.MOD ){
			if( lhsT == Type.INT && rhsT == Type.INT ){
				bo.type = Type.INT;//set the type
				return Type.INT;//returns it
			}else
				error("Error in " + bo.op + " arguments not ints");
		}
		else {
			if( lhsT == rhsT ){
				bo.type = Type.INT;//set the type
				return Type.INT;//returns it
			}else
				error("Error in " + bo.op + " arguments do not match");
		}
		return Type.INT;
	}

	@Override
	public Type visitFunCallStmt(FunCallStmt fcs) {
		if ( fcs.p == null ) {
			return null;
		}
		if ( fcs.p.params.size() != fcs.exprs.size() ) {
			error("Wrong number of args for function " + fcs.name );
			return null;
		}
		int i = 0;
		for ( Expr e : fcs.exprs ){
			Type argTsup = e.accept(this);
			Type argTreq = fcs.p.params.get(i).type;
			if ( argTsup != argTreq ){
				error("Arg" + i + " in function " + fcs.name + ": Expected " + argTreq + " but found " + argTsup );
			}
			i++;
		}
		return null;
	}

	@Override
	public Type visitWhile(While wh) {
		Type condT = wh.e.accept(this);
		if ( condT != Type.INT )
			error("The condition of while is not of type int but of type "+ condT);
		wh.stmt.accept(this);
		return null;
	}

	@Override
	public Type visitIf(If i) {
		Type condT = i.cond.accept(this);
		if ( condT != Type.INT )
			error("The condition of if is not of type int but of type "+ condT);
		i.if_.accept(this);
		if ( i.else_ != null )
			i.else_.accept(this);
		return null;
	}

	@Override
	public Type visitAssign(Assign as) {
		Type varT = as.v.accept(this);
		Type eT = as.e.accept(this);
		if (varT != eT)
			error("Assignment of "+ as.v.name + " of type "+ varT + " to expression of type " + eT );
		return null;
	}

	@Override
	public Type visitReturn(Return re) {
		if ( re.e != null ){
			Type retT = re.e.accept(this);
			if ( keepTrackOfProcForReturn.type != retT )
				error("Wrong return type for function " + keepTrackOfProcForReturn.name  + " of type " + keepTrackOfProcForReturn.type );
		}else{
			if ( keepTrackOfProcForReturn.type != Type.VOID )
				error("Empty return for function of type " + keepTrackOfProcForReturn.type );
		}
		return null;
	}

	@Override
	public Type visitVar(Var v){
		if ( v.vd == null ) {
			return null;
		}
		v.type = v.vd.type;
		return v.vd.type;
	}

}
