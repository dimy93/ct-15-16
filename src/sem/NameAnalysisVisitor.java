package sem;

import ast.*;
import java.util.ArrayList;

public class NameAnalysisVisitor extends BaseSemanticVisitor<Void> {
	protected Scope scope = null;

	@Override
	public Void visitVarDecl(VarDecl vd){
		Symbol s = scope.lookupCurrent(vd.var.name);
		if( s != null )
			error("Multiple identifiers with same name " + vd.var.name);
		else
			scope.put(new VarSymbol(vd));
		return null;
	}

	@Override
	public Void visitBlock(Block b) {
		Scope oldScope = scope;
		scope = new Scope(scope);
		// visit the children
		for( VarDecl vd : b.vds ){
			vd.accept(this);
		}
		for( Stmt stmt : b.smts ){
			stmt.accept(this);
		}
		scope = oldScope;
		return null;
	}


	@Override
	public Void visitProcedure(Procedure p) {
		Symbol s = scope.lookupCurrent(p.name);
		if( s != null )
			error("Multiple identifiers with same name " + p.name);
		else
			scope.put(new ProcSymbol(p));
		Scope oldScope = scope;
		scope = new Scope(scope);
		// visit the children
		for( VarDecl vd : p.params ){
			vd.accept(this);
		}
		for( VarDecl vd : p.block.vds ){
			vd.accept(this);
		}
		for( Stmt stmt : p.block.smts ){
			stmt.accept(this);
		}
		scope = oldScope;
		return null;
	}

	@Override
	public Void visitProgram(Program p) {
		this.scope = new Scope();
		Block empty_block = new Block(new ArrayList(),new ArrayList());
		ArrayList<VarDecl> empty_params = new ArrayList<VarDecl>();
		ArrayList<VarDecl> params_i = new ArrayList<VarDecl>();
		params_i.add(new VarDecl(Type.INT,new Var("i")));
		ArrayList<VarDecl> params_c = new ArrayList<VarDecl>();
		params_c.add(new VarDecl(Type.CHAR,new Var("c")));
		ArrayList<VarDecl> params_s = new ArrayList<VarDecl>();
		params_s.add(new VarDecl(Type.STRING,new Var("s")));
		scope.put(new ProcSymbol(new Procedure(Type.INT,"read_i",empty_params,empty_block)));
		scope.put(new ProcSymbol(new Procedure(Type.CHAR,"read_c",empty_params,empty_block)));
		scope.put(new ProcSymbol(new Procedure(Type.VOID,"print_i",params_i,empty_block)));
		scope.put(new ProcSymbol(new Procedure(Type.VOID,"print_c",params_c,empty_block)));
		scope.put(new ProcSymbol(new Procedure(Type.VOID,"print_s",params_s,empty_block)));
		for( VarDecl vd : p.varDecls ){
			vd.accept(this);
		}
		for( Procedure pr : p.procs ){
			pr.accept(this);
		}
		p.main.accept(this);
		return null;
	}

	@Override
	public Void visitVar(Var v) {
		Symbol s = scope.lookup(v.name);
		if( s == null )
			error("Undeclared variable " + v.name);
		else if ( !( s instanceof VarSymbol ) )
			error("Used function as variable");
		else
			v.vd = ((VarSymbol)s).vd;
		return null;
	}

	@Override
	public Void visitType(Type t) {
		return null;
	}

	@Override
	public Void visitIntLiteral(IntLiteral il) {
		return null;
	}

	@Override
	public Void visitStrLiteral(StrLiteral sl) {
		return null;
	}

	@Override
	public Void visitCharLiteral(ChrLiteral cl) {
		return null;
	}

	@Override
	public Void visitFunCallExpr(FunCallExpr fce) {
		Symbol s = scope.lookup(fce.name);
		if( s == null )
			error("Undeclared function " + fce.name);
		else if ( !( s instanceof ProcSymbol ) )
			error("Used variable " + fce.name + " as function");
		else
			fce.p= ((ProcSymbol)s).p;
		for ( Expr e : fce.exprs )
			e.accept(this);
		return null;
	}

	@Override
	public Void visitBinOp(BinOp binop) {
		binop.lhs.accept(this);
		binop.rhs.accept(this);
		return null;
	}

	@Override
 	public Void visitFunCallStmt(FunCallStmt fcs) {
		Symbol s = scope.lookup(fcs.name);
		if( s == null )
			error("Undeclared function " + fcs.name);
		else if ( !( s instanceof ProcSymbol ) )
			error("Used variable " + fcs.name + " as function");
		else
			fcs.p= ((ProcSymbol)s).p;
		for ( Expr e : fcs.exprs )
			e.accept(this);
		return null;
	}

	@Override
	public Void visitWhile(While wh) {
		wh.e.accept(this);
		wh.stmt.accept(this);
		return null;
	}

	@Override
	public Void visitIf(If i) {
		i.cond.accept(this);
		i.if_.accept(this);
		if ( i.else_ != null )
			i.else_.accept(this);
		return null;
	}

	@Override
	public Void visitAssign(Assign as) {
		as.v.accept(this);
		as.e.accept(this);
		return null;
	}

	@Override
	public Void visitReturn(Return re) {
		if ( re.e != null )
			re.e.accept(this);
		return null;
	}
}
