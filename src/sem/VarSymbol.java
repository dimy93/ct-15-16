package sem;
import ast.VarDecl;

public class VarSymbol extends Symbol {
	protected VarDecl vd;
	public VarSymbol(VarDecl vd) {
		super( vd.var.name );
		this.vd = vd;
	}
}
