package sem;

import java.util.HashMap;
import java.util.Map;

public class Scope {
	private Scope outer;
	private Map<String, Symbol> symbolTable;
	
	public Scope(Scope outer) { 
		this.outer = outer; 
		this.symbolTable = new HashMap<String, Symbol>();
	}
	
	public Scope() { this(null); }
	
	public Symbol lookup(String name) {
		if ( symbolTable.containsKey(name) )
			return symbolTable.get(name);
		if ( outer == null )
			return null;
		return outer.lookup(name);
	}
	
	public Symbol lookupCurrent(String name) {
		if ( symbolTable.containsKey(name) )
			return symbolTable.get(name);
		return null;
	}
	
	public void put(Symbol sym) {
		symbolTable.put(sym.name, sym);
	}
}
