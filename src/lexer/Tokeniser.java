package lexer;

import lexer.Token.TokenClass;

import java.io.EOFException;
import java.io.IOException;

/**
 * @author cdubach
 */
public class Tokeniser {

	private Scanner scanner;

	private int error = 0;

	public int getErrorCount() {
		return this.error;
	}

	public Tokeniser(Scanner scanner) {
		this.scanner = scanner;
	}

	private void error(char c, int line, int col) {
		System.out.println("Lexing error: unrecognised character (" + c + ") at " + line + ":" + col);
		error++;
	}

	public Token nextToken() {
		Token result;
		try {
			result = next();
		} catch (EOFException eof) {
			// end of file, nothing to worry about, just return EOF token
			return new Token(TokenClass.EOF, scanner.getLine(), scanner.getColumn());
		} catch (IOException ioe) {
			ioe.printStackTrace();
			// something went horribly wrong, abort
			System.exit(-1);
			return null;
		}
		return result;
	}

	/*
	 * To be completed
	 */
	private Token next() throws IOException {

		int line = scanner.getLine();
		int column = scanner.getColumn();
    String cleaned = "";

		char c = scanner.peek();

		// skip white spaces
		if (Character.isWhitespace(c)) {
			scanner.next();
			return next();
		}

		// check for char literal
		if (c == '\'') {
			try {
        String data = "";
				scanner.next();
				if (scanner.peek() == '\\') {
					scanner.next();
          char escaped = scanner.next();
          switch (escaped) {
            case 'n':  data += '\n'; break;
            case 't':  data += '\t'; break;
            case 'r':  data += '\r'; break;
            case 'b':  data += '\b'; break;
            case 'f':  data += '\f'; break;
            case '\'': data += '\''; break;
            case '\"': data += '\"'; break;
            case '\\': data += '\\'; break;
            default: error(c, line, column);return new Token(TokenClass.INVALID, line, column);
          }
				}
        if ( data.length() == 0 )
				  data += scanner.next();
				if (scanner.peek() == '\'') {
					scanner.next();
					return new Token(TokenClass.CHARACTER, data, line, column);
				}
			} catch (EOFException eof) {
				error(c, line, column);
				return new Token(TokenClass.EOF, line, column);
			}
			error(c, line, column);
			return new Token(TokenClass.INVALID, line, column);
		}
		TokenClass lastMatched = TokenClass.INVALID;
		String matched = "";
		try {
			for (matched = String.valueOf(c);; scanner.next(), matched += scanner.peek()) {
				int len = matched.length();
				if (matched.startsWith("/*")) {
					if (len < 4 || matched.charAt(len - 3) != '*' || matched.charAt(len - 2) != '/') {
						lastMatched = null;
						continue;
					}
				}
				if (matched.startsWith("//")) {
					if (len < 3 || (matched.charAt(len - 2) != '\r' && matched.charAt(len - 2) != '\n')) {
						lastMatched = null;
						continue;
					}
				}
				if (matched.startsWith("\"")) {		  
					if (len < 3 || matched.charAt(len - 2) != '\"' || matched.charAt(len - 3) == '\\') {
						lastMatched = TokenClass.STRING_LITERAL;
						continue;
					}
				}
				if (Character.isDigit(matched.charAt(0))) {
					if (Character.isDigit(matched.charAt(len - 1))) {
						lastMatched = TokenClass.NUMBER;
						continue;
					}
				}
				boolean isMatched = false;
				for (TokenClass token : TokenClass.values()) {
					if (token.getName() != null && !token.isID() && token.getName().startsWith(matched)) {
						isMatched = true;
						lastMatched = token;
						break;
					}
				}
				if (isMatched)
					continue;
				if (Character.isLetter(matched.charAt(0)) || matched.charAt(0) == '_') {
					if (Character.isLetterOrDigit(matched.charAt(len - 1)) || matched.charAt(len - 1) == '_') {
						lastMatched = TokenClass.IDENTIFIER;
						continue;
					}
				}
				matched = matched.substring(0, len - 1);
				break;
			}
		} catch (EOFException eof) {
			if ((matched.startsWith("/*") && !matched.endsWith("*/") ) || (matched.startsWith("\"") && !matched.endsWith("\"") ))  {
				lastMatched = TokenClass.EOF;
				error(c, line, column);
			}
		} finally {
      if (lastMatched == TokenClass.INVALID)
         scanner.next();
			if (lastMatched == null)
				return next();
			// Beginning of a non ID token that doesn't match entirely
			if (lastMatched.getName() != null && !lastMatched.isID() && !lastMatched.getName().equals(matched)) {
				lastMatched = TokenClass.INVALID;
			}
			if (lastMatched == TokenClass.IDENTIFIER) {
				// Check for special IDs
				for (TokenClass token : TokenClass.values()) {
					if (token.getName() != null && token.isID() && token.getName().equals(matched)) {
						lastMatched = token;
						break;
					}
				}
        if (matched.equals("read_i") || matched.equals("read_c")) {
          lastMatched = TokenClass.READ;
          cleaned = matched;
        }
        if (matched.equals("print_i") || matched.equals("print_c") || matched.equals("print_s")) {
          lastMatched = TokenClass.PRINT;
          cleaned = matched;
        }
			}
      if (lastMatched == TokenClass.IDENTIFIER || lastMatched == TokenClass.NUMBER) {
         cleaned = matched;
      }
      if (lastMatched == TokenClass.STRING_LITERAL) {
        for ( int i = 1; i < matched.length()-1; i++ ){
          if ( matched.charAt(i) == '\\' && matched.charAt(i+1) == 't'){
             cleaned += '\t';
             i++;
             continue;
             
          }
          if ( matched.charAt(i) == '\\'){
        	  i++;
	          switch ( matched.charAt(i) ) {
		          case 'n':  cleaned += '\n'; break;
		          case 't':  cleaned += '\t'; break;
		          case 'r':  cleaned += '\r'; break;
		          case 'b':  cleaned += '\b'; break;
		          case 'f':  cleaned += '\f'; break;
		          case '\'': cleaned += '\''; break;
		          case '\"': cleaned += '\"'; break;
		          case '\\': cleaned += '\\'; break;
		          default: error(c, line, column);return new Token(TokenClass.INVALID, line, column);
	          }
	          continue;
          }
          cleaned += matched.charAt(i);
        }
      }
		}

		if (lastMatched == TokenClass.INVALID) {
			// if we reach this point, it means we did not recognise a valid
			// token
			error(c, line, column);
		}
    if (cleaned.equals("")){
		  return new Token(lastMatched, line, column);
    }
    else{
      return new Token(lastMatched, cleaned, line, column);
    }
	}

}
