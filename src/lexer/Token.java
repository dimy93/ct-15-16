package lexer;

import util.Position;

/**
 * @author cdubach
 */
public class Token {

    public enum TokenClass {
        IDENTIFIER(null), // [a-zA-Z][0-9a-zA-Z_]*

        ASSIGN("="), // =

        // delimiters
        LBRA("{"), // {
        RBRA("}"), // }
        LPAR("("), // (
        RPAR(")"), // )
        SEMICOLON(";"), // ;
        COMMA(","), // ,
        
        // functions
        MAIN("main", true),  // main function identifier
        PRINT(null), // print function identifier
        READ(null),  // read function identifier

        // types
        INT("int", true),
        VOID("void", true),
        CHAR("char", true),

        // control flow
        IF("if", true),
        ELSE("else", true),
        WHILE("while", true),
        RETURN("return", true),

        // include
        INCLUDE("#include"), // #include

        // literals
        STRING_LITERAL(null), // ".*"
        NUMBER(null), // [0-9]+
        CHARACTER(null), // a character (e.g. 'a')

        // comparisons
        EQ("=="), // ==
        NE("!="), // !=
        LT("<"), // <
        GT(">"), // >
        LE("<="), // <=
        GE(">="), // >=

        // arithmetic operators
        PLUS("+"), // +
        MINUS("-"), // -
        TIMES("*"), // *
        DIV("/"), // /
        MOD("%"), // %

        // special tokens
        EOF(null),    // to signal end of file
        INVALID(null); // in case we cannot recognise a character as part of a valid token
    	private final String name;
      private final boolean id;
        TokenClass(String name) { this(name,false); }
        TokenClass(String name, boolean id ) { this.name = name; this.id = id; }
        public String getName() { return name; }
        public boolean isID() { return id; }
    }


    public final TokenClass tokenClass;
    public final String data;
    public final Position position;

    public Token(TokenClass type, int lineNum, int colNum) {
        this(type, "", lineNum, colNum);
    }

    public Token (TokenClass tokenClass, String data, int lineNum, int colNum) {
        assert (tokenClass != null);
        this.tokenClass = tokenClass;
        this.data = data;
        this.position = new Position(lineNum, colNum);
    }



    @Override
    public String toString() {
        if (data.equals(""))
            return tokenClass.toString();
        else
            return tokenClass.toString()+"("+data+")";
    }

}


